# This file is produced as a part of an educational course.
#
# A Makefile for the gnuastro-tutorial-1.sh
#
# Copyright (C) 2021-2022, Pedram Ashofteh Ardakani <pedramardakani@pm.me>
# Copyright (C) 2021-2022, Mohammad Akhlaghi <mohammad@akhlaghi.org>
#
# This file is free software: you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This file is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with Gnuastro. If not, see <http://www.gnu.org/licenses/>.

# Import user settings and make variables
include *.conf

# Create flats from list of filternames
all: final

# Make settings
#
# Always set the default shell and enable the error flag, which halts
# running the recepie when facing errors
.ONESHELL:
.SHELLFLAGS: -ec
SHELL = /bin/bash





# Download data if not present in the datasets directory, create
# symbolic links otherwise.
filtertofull = $(DOWNLOADDIR)/$(XDFPREFIX)$(1)$(XDFSUFFIX)
fitsnames := $(foreach filter,$(filternames),$(call filtertofull,$(filter)))
$(fitsnames): | $(DOWNLOADDIR)
	if ! [ -f $(DATASETSDIR)/$(@F) ]; then
	   wget $(XDFURL)/$(@F) --output-document $@
	else
	   ln -sfv $(DATASETSDIR)/$(@F) $@
	fi





# Create flat images in the deep field from datasets
filtertoflat = $(FLATDIR)/xdf-$(1).fits
flatnames := $(foreach filter,$(filternames),$(call filtertoflat,$(filter)))
$(flatnames): $(FLATDIR)/xdf-%.fits: $(call filtertofull,%) vertices.conf \
                                     | $(FLATDIR)
	astcrop $< --mode=wcs -h0 --polygon=$(DEEPFIELD) --output=$@





# Create directories
$(FLATDIR) $(DOWNLOADDIR):; mkdir -pv $@





# Create symbolic links to target directories
linknames = $(foreach name,$(FLATDIR) $(DOWNLOADDIR), $(name)-link.tmp)
links: $(linknames)
$(linknames): %-link.tmp: | $(@D)
	ln -sfv $* .$(notdir $*)



# Final target
final: $(flatnames) links


# House keeping
clean:;	rm -vf $(flatnames)
clean-download:; rm -vf $(fitsnames)
clean-links:; rm -vf .$(notdir $(FLATDIR)) .$(notdir $(DOWNLOADDIR))

clean-all: clean clean-download clean-links
